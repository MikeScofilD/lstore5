<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

class AdminController extends Controller
{
    public function __invoke(): View
    {
        return view('admin.index');
    }
//    public function index(){
////        return "Hello";
//        return view('admin.index');
//    }
}
