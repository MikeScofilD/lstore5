<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PageController extends Controller
{
    public function __invoke():View
    {
        // TODO: Implement __invoke() method.
        $products = Product::orderBy('created_at', 'desc')->take(12)->get();
//        dd($products);
        return view('welcome', compact('products'));
    }

}
