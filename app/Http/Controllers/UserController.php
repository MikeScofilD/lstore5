<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditUserRequest;
use App\Services\UserService;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserController extends Controller
{
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(): View
    {
        $users = User::paginate();

        return view('admin.users.index', compact('users'));
    }

    public function edit(User $user): View
    {
        return view('admin.users.edit', compact('user'));
    }

    public function update(EditUserRequest $request, User $user): RedirectResponse
    {
        $this->authorize('update', $user);
        $this->userService->updateUser($request, $user);
//        dd($request->all());
        $this->authorize('update', $user);


        return redirect()->route('admin.users.index');
    }

    public function delete(User $user):RedirectResponse
    {
        $this->userService->deleteUser($user);

        return redirect()->route('admin.users.index');
    }
}
