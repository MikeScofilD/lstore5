<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartDropItemRequest;
use App\Http\Requests\CartUpdateRequest;
use App\Order;
use App\Product;
use App\Services\CartService;
use \Cart;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class CartController extends Controller
{
    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function index()
    {
        return view('cart.index');
    }

    public function add($productId):RedirectResponse
    {
        $this->cartService->addProduct($productId);

        return redirect()->back();
    }

    public function update(CartUpdateRequest $request)
    {
        $this->cartService->updateCart($request);
//        Cart::update($request->productId, $request->qty);
//        Cart::update($request->productId,
//            ['name' => 'My awesome test product', 'qty' => 256, 'options' => ['size' => 'XL']]);
//        dd(Cart::content());
        return redirect()->route('cart.index');
    }

    public function drop(CartDropItemRequest $request):RedirectResponse
    {

        $this->cartService->updateCart($request);
        return redirect()->route('cart.index');
    }

    public function destroy():RedirectResponse
    {
        $this->cartService->destroyCart();
        return redirect()->route('cart.index');
    }

    public function checkout():View
    {
        return view('orders.checkout');
    }

    public function success($orderId):View
    {
        $order = Order::findOrFail($orderId);
        return view('cart.success', compact('order'));
    }
}
