<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Category extends Model
{
    public function products():BelongsTo
    {
        return $this->belongsToMany(Product::class);
    }
}
