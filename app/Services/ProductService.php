<?php


namespace App\Services;


use App\Http\Requests\ProductFormRequest;
use App\Product;

class ProductService
{
    public function storeProduct(ProductFormRequest $request):void
    {
        $product = Product::create($request->all());

        foreach ($request->categories as $categoryId) {
            $product->categories()->attach($categoryId);
        }
    }

    public function updateProduct(ProductFormRequest $request, Product $product):void
    {
        $product->update($request->all());

        foreach ($request->categories as $categoryId) {
            $product->categories()->sync($categoryId);
        }
    }

    public function deleteProduct(Product $product):void
    {
        $product->delete();
    }

    public function restoreProduct(Product $product):void
    {
        $product->restore();
    }

    public function destroyProduct(Product $product):void
    {
        $product->forceDelete();
    }
}
