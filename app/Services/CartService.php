<?php

namespace App\Services;

use App\Http\Requests\CartUpdateRequest;
use App\Product;
use \Cart;

class CartService
{
    public function addProduct(int $productId)
    {
        $product = Product::findOrfail($productId);
        $cartRow = Cart::add($product->id, $product->title, 1, $product->price);
        $cartRow->associate(Product::class);
    }

    public function updateCart(CartUpdateRequest $request){
        Cart::update($request->productId, $request->qty);
    }

    public function dropItem(CartUpdateRequest $request){
        Cart::remove($request->productId);
    }

    public function destroyCart(){
        Cart::destroy();
    }
}
